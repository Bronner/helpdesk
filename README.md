# HelpDesk

This is a REST Java application built on Spring framework.

The goal of the application is to implement basic HelpDesk features as such as:
- Ticket creation
- Conversation
- Ticket status tracking
- Email notifications

## Requirements
- Java 17+
- PostgreSQL
